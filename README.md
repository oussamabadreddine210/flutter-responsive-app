# [Flutter] Dream App - Starting page UI


## Objectif

L’objectif de ce mini projet est de développer une page bienvenue en mode responsive (UI compatible avec PC et mobile) pour l’application Dream App.
Le design ci-dessous illustre l’interface graphique de la page de bienvenue de l’App Dream sur les deux supports :
 

[UI PC](images/pc_ui.png)

[UI Mobile](images/mobile_ui.png)

⚠️ Note: la page à développer ne nécessite aucun appel aux web-services.


## Resources graphiques 

Les resources graphiques nécessaire (En format SVG) au développement de ce mini projet sont disponibles dans le dossier [images](images/)

## Prérequis

|Langage   |Framework                        | Dev pattern |  Widgets             |                         
|:--------:|:-------------------------------:|:-----------:|:--------------------:|
| Dart     | Flutter (sdk: ">=2.12.0 <3.0.0) |  BLOC       | Material components  | 


## Livraison attendue 

- Zip conteant le projet Flutter avec le code source en Dart.

## Références

- [Flutter Dart] (https://flutter.dev)
- [BLoc Pattern for Flutter Dart] (https://medium.com/flutter-community/flutter-bloc-for-beginners-839e22adb9f5)
